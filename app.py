from flask import Flask, render_template
from source.markov import getDataStats
import source.stockdata

#from ..source.markov import getDataStats

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("webapp/templates/home.html")

@app.route("/markov")
def markov(path="data/AAPL.csv"):
    data = getDataStats(path)
    return str(data)
    

if __name__ == "__main__":
    app.run(debug=True)