FROM continuumio/miniconda3


ADD . /

RUN conda env create
RUN echo "source activate stockmarkov" > ~/.bashrc
ENV PATH /opt/conda/envs/stockmarkov/bin:$PATH

RUN "./markov-run"
