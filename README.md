# StockMarkov

This project is an experiment to try out if Markov chains can be used to predict stock market rises and falls. There are a couple of strategies we're trying to employ:


1. Single data Markov chains
  * This is where we use a stock's own historical data to try to predict what's next. We look at some small subset of previous days and make a model to tell us what the next day will be.
  * For example, it may be that when Apple's stock has had large gains three days in a row we consistently see it go down the day after
2. Multi-variable Markov chains
  * This is the meat of this experiment, where we're going to look at multiple stocks and see if we can create a model from all of them.
  * There is some historical data for a couple of different tech companies, we can track not only the previous day of a certain stocks but also related stocks

## Running the program
To get started, first you'll need conda installed (miniconda will suffice). Find it [here](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)

To install the requirements, run:

`conda env create`

`source activate stockmarkov`

Now that your environment is set up, you can test the program by running:

`python markov.py`

And you should see some lines about percent accuracy

## Developing
To run all this, I'm using vscode and linux. However, this all (including the conda environment) should work on Windows. 

If you want to make changes to something, I'll go over the basic flow:
1. Clone the repo.
2. Check out a new branch. You can do that by running the following git command:

`git checkout -b my-new-branch`

3. Now that you're on your new branch, edit files, make new ones, whatever you want.
4. When you're ready, run:

`git add .`

`git commit -m "this is the comment for my commit"`

This will commit your code in your local repository.
5. Once committed, you can push your branch up to gitlab so others on the project can see it:

`git push -u origin my-new-branch`

You only need the `-u origin` the first time you push the branch, it's telling the remote repo (the one on gitlab) that you have a new branch. After that your local branch will be connected correctly and you can just do:

`git push`
when you're on the branch.
6. When you're ready for your changes to be merged into develop, go onto Gitlab and submit a new [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html). This helps the project track large branch merges and who's writing what. Don't feel like you need to wait for approval or anything, but it is a lot easier to track who is working on what when you can look at the merged merge requests and see what everyone wrote. [Here is the relevant place to submit merge requests for this project](https://gitlab.com/wraith55/stockmarkov/merge_requests)
7. Eventually a merge request might have a pipeline run before allowing a merge, but we don't have CI yet, so you should be able to just click the merge button, and you're done! Your change is now in develop.