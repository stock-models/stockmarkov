import pandas as pd

def get_stock_info(path):
    df = pd.read_csv(path, sep=',')
    df2 = df[df.columns[0:2]]
    return df2.to_json()