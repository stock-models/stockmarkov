import pandas as pd
import numpy as np
from stockdata import StockData
from mlmodels import run_ml
import sys


def usage():
    print('This program is designed to take a csv full of stock data and output statistics about the markov model we create')
    print('to use this, please provide the path to the data when you call the program')
    print('for example if you wanted data about apple stocks, you could write the following: ')
    print ('python markov.py "data/AAPL.csv"')

def main():
    if (len(sys.argv)) < 2:
        usage()

    dfs = []
    try:
        for x in range(1, len(sys.argv)):
            dfs.append(pd.read_csv(sys.argv[x], sep=','))
    except:
        usage()

    stockdata = []
    for df in dfs:
        run_ml(df, df)
        stockdata.append(StockData(df))

    for stockdatum in stockdata:
        stockdatum.exerciseMarkov()

def getDataStats(path):
    df = pd.read_csv(path, sep=',')
    data = StockData(df)
    [correct, total, hc_correct, hc_total] = data.exercise_markov()
    return {
        'num_correct' : correct,
        'num_total' : total,
        'num_high_conf_correct' : hc_correct,
        'num_high_conf_total' : hc_total,
    }

if __name__=="__main__":
    main()
