import pandas as pd

class StockData:
    @staticmethod
    def stock_df_to_percents( df ):
        return (df['Close'] - df['Open']) / df['Open']

    @staticmethod
    def numberToQuantile( percent ):
        if percent < -0.01:
            return 0
        elif percent < 0:
            return 1
        elif percent < 0.01:
            return 2
        else:
            return 3

    @staticmethod    
    def addToMarkovDict (markovdict, dictstring):
        if dictstring in markovdict:
            markovdict[dictstring] += 1    
        else:
            markovdict[dictstring] = 1
        return markovdict

    # function to make it easier to handle when a key doesn't exist
    @staticmethod
    def get_number_from_dict(dict, str):
        if str in dict:
            return dict[str]
        else:
            return 0

    # I made this a function because I was tired of writing it over and over
    @staticmethod
    def number_in_last_10_percent (self):
        return int((len(self.quantiles) - 4) -  (len(self.quantiles) - (len(self.quantiles) * 0.1)))

    # function to calculate which quantile had the highest
    @staticmethod
    def calculate_max(q1, q2, q3, q4):
        if q1 > max(q2, q3, q4):
            return 0
        elif q2 > max(q1, q3, q4):
            return 1
        elif q3 > max(q1, q2, q4):
            return 2
        elif q4 > max(q1, q2, q3):
            return 3
        else: return -1

    @staticmethod
    def calculate_high_conf(q1, q2, q3, q4, predict, total, percent):
        quants = [q1, q2, q3, q4]
        try:
            return (quants[predict] / total > percent)
        except:
            return False

   
    # this function creates the markov predictors using the first 90% of the data
    def calculate_markov( self ):
        markovq1 = {}
        markovq2 = {}
        markovq3 = {}
        markovq4 = {}
        
        for x in range(0, int(len(self.quantiles) - (len(self.quantiles) * 0.1))):
            dictstring = str(self.quantiles[x]) + str(self.quantiles[x+1]) + str(self.quantiles[x+2])
            quantile = self.quantiles[x+3]
            if quantile == 0:
                self.addToMarkovDict(markovq1, dictstring)
            elif quantile == 1:
                self.addToMarkovDict(markovq2, dictstring)
            elif quantile == 2:
                self.addToMarkovDict(markovq3, dictstring)
            else:
                self.addToMarkovDict(markovq4, dictstring)
        self.markovq1 = markovq1
        self.markovq2 = markovq2
        self.markovq3 = markovq3
        self.markovq4 = markovq4



    def __init__(self, dataframe):
        self.dataframe = dataframe
        self.percents = self.stock_df_to_percents(self.dataframe)
        self.quantiles = [self.numberToQuantile(item) for item in self.percents]
        self.calculate_markov()


    def get_quantiles(self):
        return [self.markovq1, self.markovq2, self.markovq3, self.markovq4]

    # this function tests the constructed markov chains on the remaining 10% of the data
    def exercise_markov (self):

        #initialize counters
        totalcorrect = 0
        unpredicted = 0
        highrangetotal = 0
        highrangecorrect = 0
        highconftotal = 0
        highconfcorrect = 0
        highconfpercent = 0.4

        # we're iterating through the last 10% of the data
        for x in range (int(len(self.quantiles) - (len(self.quantiles) * 0.1)), int(len(self.quantiles) - 4)):
            # the string keying in the dictionary is comprised of the scores that lead up to the day in question
            dictstring = str(self.quantiles[x]) + str(self.quantiles[x+1]) + str(self.quantiles[x+2])
            # this is the actual result (in quantile form)
            quantile = self.quantiles[x+3]

            # get the totals from the markov model
            q1total = self.get_number_from_dict(self.markovq1, dictstring)
            q2total = self.get_number_from_dict(self.markovq2, dictstring)
            q3total = self.get_number_from_dict(self.markovq3, dictstring)
            q4total = self.get_number_from_dict(self.markovq4, dictstring)

            # this is the total number of data we have for the key in question
            total = int(q1total) + int(q2total) + int(q3total) + int(q4total)

            

            # max determines which is the predicted quant
            predict = self.calculate_max(q1total, q2total, q3total, q4total)

            #highconf tracks if its a high confidence prediction or not
            highconf = self.calculate_high_conf(q1total, q2total, q3total, q4total, predict, total, highconfpercent)

            # this updates the counters according to accuracy
            if highconf:
                highconftotal += 1
            if predict == 3:
                highrangetotal += 1
            if int(quantile) == predict:
                totalcorrect += 1
                if highconf:
                    highconfcorrect += 1
            if predict == 3 and (int(quantile) == 3 or int(quantile) == 2):
                highrangecorrect += 1
            

        # right now just print out what's going on 
        print('total correct is: ' + str(totalcorrect) + ' out of: ' + str(self.number_in_last_10_percent(self)))
        print('thats ' + str(totalcorrect / self.number_in_last_10_percent(self))  + ' percent!')
        print('not accounting for unpredicted, thats: ' + str(totalcorrect) + ' out of: ' + str(self.number_in_last_10_percent(self) - unpredicted))
        print('thats ' + str(totalcorrect / (self.number_in_last_10_percent(self) - unpredicted) ) + ' percent!')
        print('highrange correct was: ' + str(highrangecorrect) + ' out of ' + str(highrangetotal))
        print('thats ' + str(highrangecorrect / highrangetotal) + ' percent!')
        print('high confidence correct was :' + str(highconfcorrect) + ' out of: ' + str(highconftotal))
        print('thats ' + str(highconfcorrect / highconftotal) + ' percent!')
        
        return [totalcorrect, self.number_in_last_10_percent(self), highrangecorrect, highrangetotal]



