from flask import Flask, make_response, jsonify
from markov import getDataStats
from stock_info import get_stock_info
import json
from flask_cors import CORS, cross_origin

import glob

application = Flask(__name__)
cors = CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

@application.route('/')
def hello_world():
    return 'Hello, World!'

@application.route('/markov/<stock>')
@cross_origin()
def markov_probs(stock):
    file = '../data/' + stock + '.csv'
    results = getDataStats(file)
    results['name'] = stock
    return jsonify(results)

@application.route('/data/<stock>')
def retrieve_stock_info(stock):
    file = '../data/' + stock + '.csv'
    info = get_stock_info(file)
    return info

@application.route('/getstocklist')
@cross_origin()
def get_stock_list():
    str_list = glob.glob("../data/*.csv")
    for x in range(len(str_list)):
        str_list[x] = str_list[x][8:-4]
    return jsonify(str_list)

if __name__ == "__main__":
    application.run(host='0.0.0.0', debug=True)