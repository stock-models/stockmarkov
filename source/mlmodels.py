import pandas as pd
import scipy
from sklearn.linear_model import LogisticRegression
from sklearn import model_selection
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC



def run_ml(data_static, data_predict):

    # example data from the machinelearningmastery website, only putting here temporarily to 
    url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
    names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
    dataset = pd.read_csv(url, names=names)

    models = []
    models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
    #models.append(('LDA', LinearDiscriminantAnalysis()))
    #models.append(('KNN', KNeighborsClassifier()))
    #models.append(('CART', DecisionTreeClassifier()))
    #models.append(('NB', GaussianNB()))
    #models.append(('SVM', SVC(gamma='auto')))

    data_static_arr = (data_static['Open'] - data_static['Close']).values[:-1]
    data_predict_arr = (data_predict['Open'] - data_predict['Close']).values[1:]

    reshape_static = data_static_arr.reshape((data_static_arr.shape[0], 1))
    reshape_predict = data_predict_arr.reshape(data_predict_arr.shape[0], 1)

    print('data_predict_arr ' + str(data_predict_arr) )

    # Split-out validation dataset
    array = dataset.values
    print('array: ' + str(array))
    X = array[:,0:4]
    Y = array[:,4]
    #print('X: ' + str(X))
    #print('Y: ' + str(Y))    
    
    validation_size = 0.20
    seed = 1
    #X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)

    #seed = 1
    X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(data_static_arr, data_predict_arr, test_size=0.2, random_state=seed)
    print('X_train before shape: ' + str(X_train))
    
    X_train = X_train.reshape(-1, 1)
    Y_train = Y_train.reshape(-1, 1)

    print('x_train after shape: ' + str(X_train))

    results = []
    names = []
    for name, model in models:
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=3, scoring='accuracy')
        results.append(cv_results)
        names.append(name)
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        print(msg)
